let css = document.querySelector('h3');
let color1 = document.querySelector('.color1');
let color2 = document.querySelector('.color2');
let body = document.getElementById('gradient');
let directions = document.getElementsByName('direction');
let gradientDirection = directions[0].value;

directions.forEach(function(direction){
	direction.onclick = () => {
        gradientDirection = direction.value;
        displayGradient();
	}
})

const displayGradient = () => {
    body.style.background = `linear-gradient(to ${gradientDirection}, ${color1.value}, ${color2.value})`;
    css.textContent = body.style.background + ';';
}

color1.oninput = () => displayGradient();
color2.oninput = () => displayGradient();
displayGradient();
